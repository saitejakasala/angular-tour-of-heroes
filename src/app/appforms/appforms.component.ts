import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';

@Component({
  selector: 'app-appforms',
  templateUrl: './appforms.component.html',
  styleUrls: ['./appforms.component.css']
})
export class AppformsComponent implements OnInit {
  name = new FormControl(null, Validators.required);
  profileForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    address: new FormGroup({
      street: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
    })
  });

  profileFormUpdated = this._fb.group({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl(''),
    address: this._fb.group({
      street: new FormControl(''),
      city: new FormControl(''),
    }),
    aliases: this._fb.array([new FormControl('', Validators.required)])
  });

  constructor(private _fb: FormBuilder) {

  }
  get aliases() {
    return this.profileFormUpdated.get('aliases') as FormArray;
  }
  ngOnInit(): void {
  }
  onSubmit() {
    console.warn(this.profileForm.value)
  }
  onSubmitUpdated() {
    console.warn(this.profileFormUpdated.value)
  }
  updateProfile() {
    this.profileForm.patchValue({
      firstName: 'Nancy',
      address: {
        street: '123 Drew Street'
      }
    });
    // this.profileForm.setValue({
    //   firstName: 'Nancy',
    //   address: {
    //     street: '123 Drew Street'
    //   }
    // });
  }
  addAlias(){
    this.aliases.push(new FormControl('',Validators.required))
  }
}
