import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Hero } from './hero';
import { MessageService } from './message.service';
import { HEROES } from './mock-heroes';
import { catchError, map, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class HeroService {
  private heroesUrl = 'api/heroes';  // URL to web api
  constructor(private _messageService: MessageService, private _httpClient: HttpClient) { }
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  getHeroes(): Observable<Hero[]> {
    return this._httpClient.get<Hero[]>(this.heroesUrl).
      pipe(
        tap(_ => this.log('HeroService: Fetched heroes')),
        catchError(this.handleError<Hero[]>('getHeroes', []))
      );
  }
  getHero(id: number): Observable<Hero> {
    return this._httpClient.get<Hero>(`${this.heroesUrl}/${id}`).
    pipe(
      tap(_ => this.log(`HeroService: Fetched hero details with id:${id}`)),
      catchError(this.handleError<Hero>('getHero'))
    );
  }
  updateHero(hero: Hero): Observable<Hero> {
    return this._httpClient.put<Hero>(`${this.heroesUrl}`,hero,this.httpOptions).
    pipe(
      tap(_ => this.log(`HeroService: Update hero details with id:${hero.id}`)),
      catchError(this.handleError<Hero>('updateHero',hero))
    );
  }
  addHero(hero:Hero){
    return this._httpClient.post<Hero>(`${this.heroesUrl}`,hero,this.httpOptions).
    pipe(
      tap(_ => this.log(`HeroService: Added hero details with id:${hero.id}`)),
      catchError(this.handleError<Hero>('addHero',hero))
    );
  }
  deleteHero(hero:Hero){
    const id = typeof hero === 'number' ? hero : hero.id;
    const url = `${this.heroesUrl}/${id}`;
  
    return this._httpClient.delete<Hero>(url,this.httpOptions).pipe(
      tap(_ => this.log(`deleted hero id=${id}`)),
      catchError(this.handleError<Hero>('deleteHero'))
    );
  }
  searchHeroes(name:string){
    return this._httpClient.get<Hero[]>(`${this.heroesUrl}?name=${name}`).
    pipe(
      tap(_ => this.log('HeroService: Searching heroes')),
      catchError(this.handleError<Hero[]>('SearchHeroes', []))
    );
  }

  private log(message: string): void {
    this._messageService.add(message);
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.log(error);

      this.log(`${operation} failed error:${error.message}`);

      return of(result as T);
    }
  }

}
