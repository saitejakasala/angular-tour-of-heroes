import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private messages: string[] = [];
  
  constructor() { }
  
  add(message: string): void {
    this.messages.push(message);
  }

  clearMessages(){
    this.messages.length=0;
  }

  getMessages(){
    return this.messages; 
  }
}
