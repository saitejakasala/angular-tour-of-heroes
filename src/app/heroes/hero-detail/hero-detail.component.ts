import { Location } from '@angular/common';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Hero } from 'src/app/hero';
import { HeroService } from 'src/app/hero.service';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {
  hero: Hero;

  constructor(private _route: ActivatedRoute,
    private _heroService: HeroService,
    private _location:Location) {

  }

  ngOnInit(): void {
    const id = +this._route.snapshot.paramMap.get('id');
    this._heroService.getHero(id)
      .subscribe(hero => {
        this.hero = hero;
        console.log(this.hero);
      });
  }
  goBack():void{
    this._location.back();
  }
  save():void{
    this._heroService.updateHero(this.hero).subscribe(x=>{
        this._location.back();
    });
  }
}
