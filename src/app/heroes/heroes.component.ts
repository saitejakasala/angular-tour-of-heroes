import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { HeroService } from '../hero.service';
import { MessageService } from '../message.service';
import { Hero } from './../hero';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit, OnDestroy {
  heroes: Hero[] = Array<Hero>();

  $heroes: Observable<Hero[]>;

  heroesSubscription = new Subscription();


  constructor(
    private _heroService: HeroService,
    private _messageService: MessageService) { }

  ngOnInit(): void {
    this.heroesSubscription = this._heroService.getHeroes().subscribe(x => {
      this.heroes = x;
    });

    this.$heroes = this._heroService.getHeroes();
  }

  onSelected(hero: Hero) {
    this._messageService.add(`Selected hero ${hero.name}`);
  }

  ngOnDestroy(): void {
    this.heroesSubscription.unsubscribe();
  }
  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this._heroService.addHero({ name } as Hero)
      .subscribe(hero => {
        this.heroes.push(hero);
      });
  }
  delete(hero: Hero) {
    this._heroService.deleteHero(hero)
      .subscribe(x => {
        this.heroes = this.heroes.filter(x => x.id != hero.id);
        console.log(this.heroes);
      });
  }
}
